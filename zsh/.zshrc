# Path to your oh-my-zsh installation.
export ZSH=$HOME/.oh-my-zsh

# Set name of the theme to load.
# Look in ~/.oh-my-zsh/themes/
# Optionally, if you set this to "random", it'll load a random theme each
# time that oh-my-zsh is loaded.
ZSH_THEME="awesomepanda"

# Uncomment the following line to use case-sensitive completion.
# CASE_SENSITIVE="true"

# Uncomment the following line to disable bi-weekly auto-update checks.
# DISABLE_AUTO_UPDATE="true"

# Uncomment the following line to change how often to auto-update (in days).
# export UPDATE_ZSH_DAYS=13

# Uncomment the following line to disable colors in ls.
# DISABLE_LS_COLORS="true"

# Uncomment the following line to disable auto-setting terminal title.
# DISABLE_AUTO_TITLE="true"

# Uncomment the following line to enable command auto-correction.
# ENABLE_CORRECTION="true"

# Uncomment the following line to display red dots whilst waiting for completion.
# COMPLETION_WAITING_DOTS="true"

# Uncomment the following line if you want to disable marking untracked files
# under VCS as dirty. This makes repository status check for large repositories
# much, much faster.
# DISABLE_UNTRACKED_FILES_DIRTY="true"

# Uncomment the following line if you want to change the command execution time
# stamp shown in the history command output.
# The optional three formats: "mm/dd/yyyy"|"dd.mm.yyyy"|"yyyy-mm-dd"
# HIST_STAMPS="mm/dd/yyyy"

# Would you like to use another custom folder than $ZSH/custom?
# ZSH_CUSTOM=/path/to/new-custom-folder

# don't check for new mail
unset MAILCHECK
MAILCHECK=0

# Which plugins would you like to load? (plugins can be found in ~/.oh-my-zsh/plugins/*)
# Custom plugins may be added to ~/.oh-my-zsh/custom/plugins/
# Example format: plugins=(rails git textmate ruby lighthouse)
# Add wisely, as too many plugins slow down shell startup.
plugins=(git sublime osx)

source $ZSH/oh-my-zsh.sh

# User configuration

# export MAMP_PHP=/Applications/MAMP/bin/php/php5.6.10/bin
# export PATH="$MAMP_PHP:$PATH"

export PATH="/usr/local/bin:/usr/bin:/bin:/usr/sbin:/sbin:/opt/X11/bin"
# export MANPATH="/usr/local/man:$MANPATH"

# You may need to manually set your language environment
# export LANG=en_US.UTF-8

# Preferred editor for local and remote sessions
# if [[ -n $SSH_CONNECTION ]]; then
#   export EDITOR='vim'
# else
#   export EDITOR='mvim'
# fi

# Compilation flags
# export ARCHFLAGS="-arch x86_64"

# ssh
# export SSH_KEY_PATH="~/.ssh/dsa_id"

# Set personal aliases, overriding those provided by oh-my-zsh libs,
# plugins, and themes. Aliases can be placed here, though oh-my-zsh
# users are encouraged to define aliases within the ZSH_CUSTOM folder.
# For a full list of active aliases, run `alias`.
#
# Example aliases
# alias zshconfig="st ~/.zshrc"
# alias ohmyzsh="st ~/.oh-my-zsh"

. `brew --prefix`/etc/profile.d/z.sh

alias w="cd ~/Workspace"
alias wf="cd ~/Workspace/Freelance"

alias ws="cd ~/Workspace/Webstars/Clients/Webstars/Website\ 2014/Build"

alias ots="cd ~/Workspace/Freelance/OutToSea/OTS/Build/wp-content/themes/ots"
alias pas="cd ~/Workspace/Freelance/OutToSea/Peggs\ and\ Son/"
alias urc="cd ~/Workspace/Freelance/OURC/Site/Build"

alias mrb="cd ~/Workspace/Freelance/Mr\ Bongo/Build/"
alias mml="cd ~/Workspace/Projects/Mister\ Mist/Build_3.0.1/"
alias mm="cd ~/Workspace/Projects/mistermist"

alias wpddb="wp deploy push dev --what=db"
alias wpdt="wp deploy push dev --what=themes"
alias wpdp="wp deploy push dev --what=plugins"
alias wpda="wp deploy push dev --what=db && wp deploy push dev --what=themes && wp deploy push dev --what=plugins && wp deploy push dev --what=uploads"

alias bru="brew update; brew upgrade"
alias gu="gem update"

alias watb="cd ~/Workspace/WATB"

alias ssh-watb="ssh watb@178.62.69.131"

alias sass-pack="git archive --remote git@gitlab.com:paulmist/sass-pack.git -o sasspack.zip HEAD && tar -xvf sasspack.zip && rm sasspack.zip"

export TRELLO_APP_KEY='464e394f981eb7568437b3c3fca3d6ae'
export TRELLO_APP_SECRET='1eb29125b9b5c4c5fae53eb1edc2d56b39e55de0e35df100bc83170f0a821db2'
export TRELLO_AUTH_TOKEN='b51c400ae4faa07294556b762c2cd2cb02614d5e3b14f8d344b78721fe533533'

##
# WP-CLI / XAMPP compat
##

#echo $PATH | grep -q -s "/Applications/MAMP/bin/php/php5.6.27/bin"
#if [ $? -eq 1 ] ; then
#	export XAMPP_PATH=/Applications/MAMP/bin/php/php5.6.27/bin
#	export PATH="$XAMPP_PATH:$PATH"
#fi
#export PATH=$PATH:/Applications/MAMP/bin:/Applications/MAMP/Library/bin:/usr/local/bin:/usr/newage/bin

#PHP_VERSION=$(ls /Applications/MAMP/bin/php/ | sort -n | tail -1)
#export PATH=/Applications/MAMP/bin/php/${PHP_VERSION}/bin:$PATH

#PHP_VERSION=ls /Applications/MAMP/bin/php/ | sort -n | tail -1
#export PATH=/Applications/MAMP/bin/php/${PHP_VERSION}/bin:$PATH

#MAMP Madness
export PATH=/Applications/MAMP/Library/bin:$PATH
PHP_VERSION=`ls /Applications/MAMP/bin/php/ | sort -n | tail -1`
export PATH=/Applications/MAMP/bin/php/${PHP_VERSION}/bin:$PATH

export PATH="$PATH:$HOME/.rvm/bin" # Add RVM to PATH for scripting
[ -s "$NVM_DIR/bash_completion" ] && \. "$NVM_DIR/bash_completion"  # This loads nvm bash_completion

export NVM_DIR="$HOME/.nvm"
[ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh"  # This loads nvm
export PATH="/usr/local/sbin:$PATH"
